const { Router } = require('express');
const { getAllPosts, getPostById, createNewPost, updatePost, deletePost } = require('../controllers/PostsControllers');

const router = Router();

// 게시글 목록
router.get('/', getAllPosts);

// 게시글 상세
router.get('/:shortId', getPostById);

// 게시글 등록
router.post('/', createNewPost);

// 게시글 업데이트
router.patch('/:shortId', updatePost);

// 게시글 삭제
router.delete('/:shortId', deletePost);

module.exports = router;