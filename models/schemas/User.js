const { Schema } = require('mongoose');
const { shortId } = require('./type/short-id');

exports.UserSchema = new Schema({
    shortId,
    email: String,
    name: String,
    password: String,
    // required: true,
}, {
    timestamps: true,
})