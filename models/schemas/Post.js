const { Schema, Mongoose } = require('mongoose');
const { shortId } = require('./type/short-id');

exports.PostSchema = new Schema({
    shortId,
    title: String,
    content: String,
    author: String, // after connecting passport, make a connection with User model.
}, {
    timestamps: true,
})