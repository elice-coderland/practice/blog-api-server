const express = require('express');
const mongoose = require('mongoose');
const postsRouter = require('./routes/postsRouter'); // -_-
const app = express();
const PORT = 8080;

app.use(express.json());
// app.use('/', indexRouter);
app.get('/', (req, res) => {
    res.send('hello blog')
})

app.use('/posts', postsRouter)

app.listen(PORT, () => {
    console.log('The Server is listening....');
})