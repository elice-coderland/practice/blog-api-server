const { Post } = require("../models");

const asyncHandler = require('../utils/asyncHandler');

exports.getAllPosts = asyncHandler(async (req, res) => {
    const page = Number(req.query.page || 1);
    const perPage = Number(req.query.perPage || 10);

    const [totalPosts, posts] = await Promise.all([
        Post.countDocuments({}),
        Post.find({})
            .sort({ createdAt: -1 })
            .skip(perpage * (page - 1))
            .limit(perPage)
    ]);
    
    const totalPages = Math.ceil(totalPosts / perPage);
    res.send('GET ALL POSTS');
});

exports.getPostById = asyncHandler(async (req, res) => {
    const { shortId } = req.params;
    const postById = await Post.findOne({ shortId });
    if (!postById) throw new Error('There is no such post with the id.');

    res.send('postById');
});

exports.createNewPost = asyncHandler(async (req, res) => {
    const { title, content } = req.body;
    const newPost = await Post.create({ title, content }); // passport 이후, user 연결

    res.send('New Post');
});

exports.updatePost = asyncHandler(async (req, res) => {
    const { shortId } = req.params;
    const { title, content } = req.body;

    const updatedPost = await Post.findOneAndUpdate({ shortId }, { title, content });

    res.status(204).send('UPDATE POST');
});

exports.deletePost = asyncHandler(async (req, res) => {
    const { shortId } = req.params;
    
    await Post.findOneAndDelete({ shortId });

    res.status(204).send('DELETE POST');
});